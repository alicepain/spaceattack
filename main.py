import pygame
import sys
import numpy as np

screenDims = (1200, 800)

class Graphics():
    background = "spaceBackground.jpeg"
    spaceship = "spaceship2.png"
    fireball = "fireball3.png"

class Fireball():

    def __init__(self, game, spaceship):
        self.game = game
        self.spaceship = spaceship
        self.surface = pygame.transform.scale(pygame.image.load(Graphics.fireball), (30, 30)).convert_alpha()
        self.rect = self.surface.get_rect(center = np.array(screenDims)/2)
        self.shooted = False
        self.sound = pygame.mixer.Sound('burst.wav')
        self.i = 0

    def resetPos(self):
        self.rect[0] = screenDims[0]//2
        self.rect[1] = screenDims[1]//2

    def isOutOfFrame(self):
        return self.rect[0] > screenDims[0]//2 or self.rect[1] < -screenDims[1]//2 or self.rect[0] < -screenDims[0]//2 or self.rect[1] > screenDims[1]//2
    
    def shoot(self, aim):
        self.shooted = True
        self.aim = aim
        self.sound.play()

    def draw(self, mouse):
        if self.shooted:
            self.i += 1
            self.rect[0] -= self.aim[0] // self.aim[1] * 10
            self.rect[1] -= 10
            self.game.screen.blit(self.surface, self.rect)

        if self.isOutOfFrame():
            self.shooted = False
            self.resetPos()
            self.spaceship.fireballs.remove(self)

class Spaceship():
    
    def __init__(self, game):
        self.game = game
        self.surface = pygame.transform.scale(pygame.image.load(Graphics.spaceship), (150, 150)).convert_alpha()
        self.rect = self.surface.get_rect(center = np.array(screenDims)/2) 
        self.fireballs = []
        self.aim = 0

    def rotate(self, mouse):
        if mouse[0] > 0:
            if mouse[0]: 
                angle = np.arctan(mouse[1]/-mouse[0]) * 180/np.pi + 90
            else:
                angle = np.arctan(mouse[1]) * 180/np.pi + 90
        else:
            if mouse[0]: 
                angle = np.arctan(mouse[1]/-mouse[0]) * 180/np.pi - 90
            else:
                angle = np.arctan(mouse[1]) * 180/np.pi - 90
        
        rotatedSurface = pygame.transform.rotate(self.surface, angle)
        return rotatedSurface

    def draw(self, mouse):
        rotatedSurface = self.rotate(mouse)
        self.game.screen.blit(rotatedSurface, self.rect)

    def drawFireballs(self, mouse):
        for fireball in self.fireballs:
            fireball.draw(mouse)

    def shoot(self, aim):
        fireball = Fireball(self.game, self)
        self.fireballs.append(fireball)
        fireball.shoot(aim)

class Game():

    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode(screenDims)
        self.clock = pygame.time.Clock()
        self.mainBackground = pygame.transform.scale(pygame.image.load(Graphics.background).convert(), (screenDims))
        self.backgroundImages = [
                self.mainBackground,
                pygame.transform.flip(self.mainBackground, True, False),
                pygame.transform.flip(self.mainBackground, False, True),
                pygame.transform.flip(self.mainBackground, True, True)
                ]
        self.backgroundPoses = [
                -np.array(screenDims),
                np.array([0, -screenDims[1]]),
                np.array([screenDims[0], -screenDims[1]]),
                np.array([screenDims[0] * 2, -screenDims[1]]),
                np.array([-screenDims[0], 0]),
                np.array([0, 0]),
                np.array([screenDims[0], 0]),
                np.array([screenDims[0] * 2, 0]),
                np.array([-screenDims[0], screenDims[1]]),
                np.array([0, screenDims[1]]),
                np.array(screenDims),
                np.array([screenDims[0] * 2, screenDims[1]]),
                np.array([-screenDims[0], screenDims[1] * 2]),
                np.array([0, screenDims[1] * 2]),
                np.array([screenDims[0], screenDims[1] * 2]),
                np.array([screenDims[0] * 2, screenDims[1] * 2])
                ]
        backgroundOrder = [3, 2, 3, 2, 1, 0, 1, 0, 3, 2, 3, 2, 1, 0, 1, 0]
        self.backgrounds = [self.backgroundImages[i] for i in backgroundOrder]
        self.spaceship = Spaceship(self)

    def draw(self, mouse):
        self.screen.fill((255,255,255))
        for i, background in enumerate(self.backgrounds):
            self.screen.blit(background, self.backgroundPoses[i])
        self.spaceship.draw(mouse)
        self.spaceship.drawFireballs(mouse)
        pygame.display.update()

    def slideBackgrounds(self, mouse):
        for pos in self.backgroundPoses:
            pos += mouse//80

    def translateBackgrounds(self):
        for i, pos in enumerate(self.backgroundPoses):
            if pos[0] > screenDims[0]:
                self.backgroundPoses[i] -= np.array([screenDims[0] * 4, 0])
            if pos[1] > screenDims[1]:
                self.backgroundPoses[i] -= np.array([0, screenDims[1] * 4])
            if pos[0] < -screenDims[0]:
                self.backgroundPoses[i] += np.array([screenDims[0] * 4, 0])
            if pos[1] < -screenDims[1]:
                self.backgroundPoses[i] += np.array([0, screenDims[1] * 4])

    def run(self):
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        aim = np.array(screenDims) // 2 - np.array(pygame.mouse.get_pos())
                        self.spaceship.shoot(aim)

            mouse = np.array(screenDims) // 2 - np.array(pygame.mouse.get_pos())
            self.slideBackgrounds(mouse)
            self.translateBackgrounds()
            self.draw(mouse)
            self.clock.tick(120)

if __name__ == "__main__":
    game = Game()
    game.run()

